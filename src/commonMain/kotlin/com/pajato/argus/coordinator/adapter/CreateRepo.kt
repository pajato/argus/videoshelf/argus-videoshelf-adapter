package com.pajato.argus.coordinator.adapter

import com.pajato.argus.coordinator.adapter.video.filterMaybe
import com.pajato.argus.coordinator.core.Timestamp
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import kotlinx.serialization.serializer
import java.net.URI
import java.util.TreeMap
import kotlin.collections.List
import kotlin.collections.MutableMap
import kotlin.collections.set
import kotlin.collections.toList

internal suspend fun createRepo(dir: URI, shelf: Shelf): VideoRepo {
    val repo = getRepo(dir, shelf)
    if (shelf.path.isNotEmpty()) repo.injectDependency(getUri(dir, shelf))
    return repo
}

private fun getUri(dir: URI, shelf: Shelf): URI = URI("$dir/${shelf.path}")

private fun getRepo(dir: URI, shelf: Shelf): VideoRepo {
    return object : VideoRepo {
        private var repoUri: URI? = null
        override val cache: TreeMap<Timestamp, Video> = TreeMap<Timestamp, Video>()
        override val id: String = shelf.id
        override var selectedItem: Video? = null
        override suspend fun injectDependency(uri: URI) { loadCache(dir, shelf, cache).also { repoUri = uri } }
        override fun register(item: Video) { register(shelf, cache, repoUri, item) }
        override fun register(json: String) { register(shelf, cache, repoUri, json) }
        override fun remove(item: Video) { remove(shelf, cache, repoUri, item) }
        override fun select(item: Video) { selectedItem = item }
        override fun filter(filter: Filter, infoRepo: InfoRepo): List<Video> =
            filterMaybe(filter, infoRepo, cache.values.toList())
        override fun filter(filter: Filter, infoRepo: InfoRepo, videos: List<Video>): List<Video> =
            filterMaybe(filter, infoRepo, videos)
    }
}

private fun loadCache(dir: URI, shelf: Shelf, cache: MutableMap<Timestamp, Video>) {
    val uri = getUri(dir, shelf)
    fun getKeyFromItem(item: Video) = item.timestamp
    validateRepo(uri)
    readAndPruneData(uri, cache, serializer(), ::getKeyFromItem)
}

private fun register(shelf: Shelf, cache: MutableMap<Timestamp, Video>, repoUri: URI?, json: String) {
    jsonFormat.decodeFromString(Video.serializer(), json).also { cache[it.timestamp] = it }
    if (shelf.path.isNotEmpty()) persist(repoUri!!, json)
}

private fun register(shelf: Shelf, cache: MutableMap<Timestamp, Video>, repoUri: URI?, item: Video) {
    cache[item.timestamp] = item
    if (shelf.path.isNotEmpty()) persist(repoUri!!, jsonFormat.encodeToString(Video.serializer(), item))
}

private fun remove(shelf: Shelf, cache: MutableMap<Timestamp, Video>, repoUri: URI?, item: Video) {
    cache.remove(item.timestamp)
    if (shelf.path.isNotEmpty()) persist(repoUri!!, "-${jsonFormat.encodeToString(Video.serializer(), item)}")
}
