package com.pajato.argus.coordinator.adapter

import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import java.net.URI

internal var shelfUri: URI? = null

internal suspend fun setupCache(dir: URI, cache: MutableMap<String, Coordinator>, shelfCache: MutableMap<String, Shelf>) {
    validateDir(dir).also { shelfUri = URI("$dir/shelf.txt") }
    readAndPruneData(shelfUri!!, shelfCache, Shelf.serializer(), ::getIdFromShelf)
    shelfCache.values.forEach { cache[it.id] = Coordinator(it, createRepo(dir, it)) }
}

private fun getIdFromShelf(shelf: Shelf): String = shelf.id

internal fun setupOrderList(cache: MutableMap<String, Coordinator>, orderList: MutableList<String>) {
    orderList.clear()
    orderList.addAll(MutableList(cache.size) { "" })
    cache.values.forEach { orderList[it.shelf.order] = it.shelf.id }
}

internal fun swapOrder(cache: MutableMap<String, Coordinator>, orderList: MutableList<String>, index1: Int, index2: Int) {
    val (id1, id2) = Pair(orderList[index1], orderList[index2])
    orderList[index1] = id2
    orderList[index2] = id1
    update(cache, id1, index2)
    update(cache, id2, index1)
}

private fun update(cache: MutableMap<String, Coordinator>, id: String, index: Int) {
    val entry = cache[id]!!
    val shelf = entry.shelf.copy(order = index)
    cache[id] = entry.copy(shelf = shelf)
    shelfUri?.let { persistShelf(it, shelf) }
}

private fun persistShelf(uri: URI, shelf: Shelf) { persist(uri, jsonFormat.encodeToString(Shelf.serializer(), shelf)) }

internal fun reset(cache: MutableMap<String, Coordinator>) {
    cache.clear()
    shelfUri = null
}
