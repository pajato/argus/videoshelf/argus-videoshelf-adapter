package com.pajato.argus.coordinator.adapter

import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.CoordinatorRepo
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_NOT_FOUND
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import java.net.URI

/**
 * ArgusCoordinatorRepo is a singleton object that provides functionality to manage
 * and interact with coordinators, shelves, and video repositories within a specific directory structure.
 * It implements the CoordinatorRepo interface.
 */
public object ArgusCoordinatorRepo : CoordinatorRepo {
    override val cache: MutableMap<String, Coordinator> = mutableMapOf()
    override val orderList: MutableList<String> = mutableListOf()

    override fun getShelf(id: String): Shelf =
        (cache[id] ?: throw CoordinatorError(get(COORDINATOR_NOT_FOUND, Arg("id", id)))).shelf

    override fun getVideoRepo(id: String): VideoRepo =
        (cache[id] ?: throw CoordinatorError(get(COORDINATOR_NOT_FOUND, Arg("id", id)))).repo

    override suspend fun injectDependencies(dir: URI, infoRepo: InfoRepo) {
        val shelfCache: MutableMap<String, Shelf> = mutableMapOf()
        setupCache(dir, cache, shelfCache)
        setupOrderList(cache, orderList)
    }

    override fun swapWithShelfAbove(id: String) {
        val index = orderList.indexOf(id)
        if (index > 0) swapOrder(cache, orderList, index, index - 1)
    }

    override fun swapWithShelfBelow(id: String) {
        val lastIndex = orderList.size - 1
        val index = orderList.indexOf(id)
        if (index < lastIndex) swapOrder(cache, orderList, index, index + 1)
    }
}
