package com.pajato.argus.coordinator.adapter.video

import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.uc.CommonUseCases

private data class FilterParams(
    val infoRepo: InfoRepo,
    val genreIds: List<Int>,
    val networkIds: List<Int>,
    val profileIds: List<Int>,
)

public fun filterMaybe(filter: Filter, infoRepo: InfoRepo, videos: List<Video>): List<Video> = with(filter) {
    val params = FilterParams(infoRepo, genreIds, networkIds, profileIds)
    videos.filter { it.accept(params) }
}

private fun Video.accept(params: FilterParams): Boolean {
    val noFilters = params.genreIds.isEmpty() && params.networkIds.isEmpty() && params.profileIds.isEmpty()
    return noFilters || this.acceptAll(params, params.infoRepo)
}

private fun Video.acceptAll(params: FilterParams, infoRepo: InfoRepo): Boolean = when {
    params.genreIds.isNotEmpty() && params.genreIds.isSuperSetOf(this, infoRepo).not() -> false
    params.networkIds.isNotEmpty() && params.networkIds.contains(networkId).not() -> false
    params.profileIds.isNotEmpty() && params.profileIds.contains(profileId).not() -> false
    else -> true
}

private fun List<Int>.isSuperSetOf(video: Video, infoRepo: InfoRepo): Boolean {
    val key: InfoKey = if (video.isTv()) InfoKey(Tv.name, video.infoId) else InfoKey(Movie.name, video.infoId)
    val videoGenres: Set<Int> = CommonUseCases.getVideoGenres(infoRepo, key).toSet()
    val genreSet: Set<Int> = this.toSet()
    return genreSet.all { it in videoGenres }
}
