package com.pajato.argus.coordinator.adapter

import com.pajato.argus.coordinator.adapter.video.TestInfoRepo
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.persister.jsonFormat
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import java.net.URL
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class CreateRepoUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val id = Shelves.History.name
    private val continueId = Shelves.Next.name
    private val name = "files"
    private val uri = loader.getResource(name)?.toURI() ?: fail("Could not load the valid mono video shelf repo")
    private val expectedSize = 1122

    @BeforeTest fun setUp() {
        reset(ArgusCoordinatorRepo.cache)
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { ArgusCoordinatorRepo.injectDependencies(uri, TestInfoRepo) }
    }

    @Test fun `When multiple videos are loaded, verify the video cache properties`() { assertCoordinator() }

    private fun assertCoordinator() {
        val id = Shelves.History.name
        val repo = ArgusCoordinatorRepo.getVideoRepo(id)
        assertEquals(expectedSize, repo.cache.size)
    }

    @Test fun `When registering a continue watching video, verify the cache size only`() {
        val repo = ArgusCoordinatorRepo.getVideoRepo(continueId)
        repo.register(Video(0L))
        assertEquals(1, repo.cache.size)
    }

    @Test fun `When registering a continue watching video using json, verify the cache size only`() {
        val repo = ArgusCoordinatorRepo.getVideoRepo(continueId)
        repo.register(jsonFormat.encodeToString(Video.serializer(), Video(0L)))
        assertEquals(1, repo.cache.size)
    }

    @Test fun `When registering a persisting video, verify the cache size and the persisted state`() {
        val repo = ArgusCoordinatorRepo.getVideoRepo(id)
        repo.register(Video(0L))
        assertEquals(expectedSize + 1, repo.cache.size)
        assertPersisted()
    }

    private fun assertPersisted() {
        val coordinator = ArgusCoordinatorRepo.cache[id] ?: fail("Could not load the coordinator resource file!")
        val path = "$name/${coordinator.shelf.path}"
        val repoUrl: URL = loader.getResource(path) ?: fail("Could not load the repo resource file!")
        assertEquals(expectedSize + 1, File(repoUrl.toURI()).readLines().size)
    }

    @Test fun `When removing a persisting video, verify the cache size and the persisted state`() {
        val repo = ArgusCoordinatorRepo.getVideoRepo(id)
        repo.remove(Video(-1L))
        assertEquals(expectedSize - 1, repo.cache.size)
        assertPersisted()
    }

    @Test fun `When removing a continue watching video, verify the cache size only`() {
        val repo = ArgusCoordinatorRepo.getVideoRepo(continueId)
        repo.register(Video(0L))
        repo.remove(Video(0L))
        assertEquals(0, repo.cache.size)
    }

    @Test fun `When selecting a video, verify the behavior`() {
        val repo = ArgusCoordinatorRepo.getVideoRepo(id)
        val item = repo.cache[-1L] ?: fail("Expected video is not cached!")
        repo.select(item)
        assertEquals(-1L, repo.selectedItem?.timestamp ?: fail("No video is selected!"))
    }
}
