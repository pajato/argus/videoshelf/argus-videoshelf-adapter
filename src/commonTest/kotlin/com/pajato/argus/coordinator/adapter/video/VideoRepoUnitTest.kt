package com.pajato.argus.coordinator.adapter.video

import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.getVideoRepo
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.injectDependencies
import com.pajato.argus.coordinator.adapter.reset
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class VideoRepoUnitTest : ReportingTestProfiler() {
    private val loader = this.javaClass.classLoader
    private val id = Shelves.History.name
    private val expectedSize = 1123

    @BeforeTest fun setUp() {
        reset(ArgusCoordinatorRepo.cache)
        copyResourceDirs(loader, "read-only-files", "files")
    }

    @Test fun `When injecting a file rather than a dir URI, verify the CoordinatorError exception`() {
        val path = "invalid-video-repo/dummy"
        val uri = loader.getResource(path)?.toURI() ?: fail(DIR_ERROR)
        runBlocking { assertFailsWith<CoordinatorError> { injectDependencies(uri, TestInfoRepo) } }
    }

    @Test fun `When accessing a video repo using an invalid id, verify the exception`() {
        val invalidId = "com.invalid.id"
        val expected: String = StringsResource.get(ErrorStrings.COORDINATOR_NOT_FOUND, Arg("id", invalidId))
        fun assert(error: CoordinatorError): Unit = assertEquals(expected, error.message)
        runBlocking { assertFailsWith<CoordinatorError> { getVideoRepo(invalidId) }.also { assert(it) } }
    }

    @Test fun `When registering using JSON after injecting a valid URI, verify correct behavior`() {
        val uri = loader.getResource("files")?.toURI() ?: fail("Could not load the resource 'files'!")
        val itemUrl = loader.getResource("item.json") ?: fail("Could not load the resource 'item.json'!")
        val json = itemUrl.readText().trim()
        runBlocking { injectDependencies(uri, TestInfoRepo) }
        getVideoRepo(id).register(json)
        assertCache(getVideoRepo((id)))
    }

    private fun assertCache(repo: VideoRepo) {
        val item = repo.cache[0] ?: fail("Item with id == 0 failed to register!")
        assertEquals(expectedSize, repo.cache.size)
        assertEquals(0, item.timestamp)
        assertEquals(29339, item.infoId)
        assertEquals(-1, item.series)
        assertEquals(0, item.episode)
        assertEquals(102, item.profileId)
        assertEquals(1024, item.networkId)
    }

    companion object {
        const val DIR_ERROR = "Could not create the base test directory!"
    }
}
