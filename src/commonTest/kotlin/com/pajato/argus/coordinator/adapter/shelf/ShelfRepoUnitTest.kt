package com.pajato.argus.coordinator.adapter.shelf

import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.injectDependencies
import com.pajato.argus.coordinator.adapter.reset
import com.pajato.argus.coordinator.adapter.video.TestInfoRepo
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class ShelfRepoUnitTest : ReportingTestProfiler() {
    private val loader = this.javaClass.classLoader

    @BeforeTest fun setUp() {
        reset(ArgusCoordinatorRepo.cache)
        copyResourceDirs(loader, "read-only-files", "files")
    }

    @Test fun `When injecting a non-file URI, verify the exception`() {
        val uri = this::class.java.classLoader.getResource("invalid-shelf-repo")?.toURI() ?: fail(DIR_ERROR)
        val expected = get(ErrorStrings.COORDINATOR_NOT_A_FILE, Arg("path", "$uri/shelf.txt"))
        fun assert(error: CoordinatorError) = assertEquals(expected, error.message)
        runBlocking { assertFailsWith<CoordinatorError> { injectDependencies(uri, TestInfoRepo) }.also { assert(it) } }
    }

    @Test fun `When injecting a valid file URI that does not exist, verify the CoordinatorError exception`() {
        val path = "files"
        val uri = this::class.java.classLoader.getResource(path)?.toURI() ?: fail(DIR_ERROR)
        val expected = get(ErrorStrings.COORDINATOR_EXISTS_ERROR, Arg("path", "$uri/shelf.txt"))
        fun assert(error: CoordinatorError) = assertEquals(expected, error.message)
        if (!File(URI("$uri/shelf.txt")).delete()) fail(DELETE_ERROR)
        runBlocking { assertFailsWith<CoordinatorError> { injectDependencies(uri, TestInfoRepo) }.also { assert(it) } }
    }

    @Test fun `When accessing a shelf using an invalid id, verify the exception`() {
        val shelfRepo = ArgusCoordinatorRepo
        val invalidId = "com.invalid.id"
        val expected = get(ErrorStrings.COORDINATOR_NOT_FOUND, Arg("id", invalidId))
        assertFailsWith<CoordinatorError> { shelfRepo.getShelf(invalidId) }.also { assertEquals(expected, it.message) }
    }

    @Test fun `When injecting a valid file URI that is empty, verify the CoordinatorError exception`() {
        val dirPath = "empty-shelf-dir"
        val filePath = "shelf.txt"
        val uri = loader.getResource(dirPath)?.toURI() ?: fail(DIR_ERROR)
        val expected = get(ErrorStrings.COORDINATOR_EMPTY_ERROR, Arg("path", "$uri/$filePath"))
        fun assert(error: CoordinatorError) = assertEquals(expected, error.message)
        runBlocking { assertFailsWith<CoordinatorError> { injectDependencies(uri, TestInfoRepo) }.also { assert(it) } }
    }

    companion object {
        const val DIR_ERROR = "Could not create the base test directory!"
        const val DELETE_ERROR = "Could not delete the resource file 'files/shelf.txt'"
    }
}
