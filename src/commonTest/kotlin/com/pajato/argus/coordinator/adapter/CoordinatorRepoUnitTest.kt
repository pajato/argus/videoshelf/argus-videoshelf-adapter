package com.pajato.argus.coordinator.adapter

import com.pajato.argus.coordinator.adapter.video.TestInfoRepo
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class CoordinatorRepoUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val id = "TestShelf"

    @BeforeTest fun setUp() { reset(ArgusCoordinatorRepo.cache) }

    @Test fun `When dependencies have not been injected, verify the CoordinatorError exception`() {
        val repo = ArgusCoordinatorRepo
        assertEquals(null, shelfUri)
        assertEquals(0, repo.cache.size)
    }

    @Test fun `When injecting dependencies using a flat file, verify the exception`() {
        val name = "empty.txt"
        val uri = loader.getResource(name)?.toURI() ?: fail("Could not load the valid mono video shelf repo")
        val expected = StringsResource.get(ErrorStrings.COORDINATOR_DIR_ERROR, Arg("dir", "$uri"))
        assertFailsWith<CoordinatorError> { runBlocking { ArgusCoordinatorRepo.injectDependencies(uri, TestInfoRepo) } }
            .also { assertEquals(expected, it.message) }
    }

    @Test fun `When a single, valid test shelf in injected, verify the shelf and video repo properties`() {
        val name = "valid-mono-repo"
        val uri = loader.getResource(name)?.toURI() ?: fail("Could not load the valid mono video shelf repo")
        runBlocking { ArgusCoordinatorRepo.injectDependencies(uri, TestInfoRepo) }
        assertCoordinator()
    }

    private fun assertCoordinator() {
        val shelf = ArgusCoordinatorRepo.getShelf(id)
        val repo = ArgusCoordinatorRepo.getVideoRepo(id)
        assertEquals("/test.txt", shelf.path)
        assertEquals(id, repo.id)
        assertEquals(0, repo.cache.size)
        assertEquals(null, repo.selectedItem)
    }
}
