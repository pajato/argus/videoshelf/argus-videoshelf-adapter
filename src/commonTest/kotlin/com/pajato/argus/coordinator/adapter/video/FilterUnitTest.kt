package com.pajato.argus.coordinator.adapter.video

import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo
import com.pajato.argus.coordinator.adapter.ArgusCoordinatorRepo.injectDependencies
import com.pajato.argus.coordinator.adapter.reset
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class FilterUnitTest : ReportingTestProfiler() {
    private val id = Shelves.History.name
    private val loader = this.javaClass.classLoader
    private val uri: URI = loader.getResource("files")?.toURI() ?: fail("Could not load the resource dir!")

    private val repo: VideoRepo get() = ArgusCoordinatorRepo.getVideoRepo(id)

    private val infoId = 1911
    private val genreId1 = 35 // Comedy
    private val genreId2 = 18 // Drama
    private val networkId = 224
    private val profileId = 6
    private val video1 = Video(1L, infoId)
    private val video2 = Video(2L, networkId = networkId)
    private val video4 = Video(4L, profileId = profileId)
    private val video6 = Video(6L, 29339, series = -1, profileId = profileId)
    private val video7 = Video(7L, networkId = networkId, profileId = profileId)
    private val expectedSize = 1122

    @BeforeTest fun setUp() {
        reset(ArgusCoordinatorRepo.cache)
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking {
            injectDependencies(uri, TestInfoRepo)
            TestInfoRepo.injectDependency(getUri())
        }
    }

    @Test fun `When filtering an empty list, verify the result is an empty list`() {
        val filter = Filter()
        assertEquals(emptyList(), ArgusCoordinatorRepo.getVideoRepo(id).filter(filter, TestInfoRepo, emptyList()))
    }

    @Test fun `When filtering using the default list, verify the result size`() {
        val filter = Filter()
        assertEquals(expectedSize, ArgusCoordinatorRepo.getVideoRepo(id).filter(filter, TestInfoRepo).size)
    }

    @Test fun `When the list being filtered is an empty list, verify the result is also an empty list`() {
        val filter = Filter()
        val result = repo.filter(filter, TestInfoRepo, emptyList())
        assertTrue(result.isEmpty(), FILTER_NON_EMPTY)
    }

    @Test fun `When filtering only with empty text, verify the result is also the input list`() {
        val filter = Filter()
        val list = listOf(video1, video6)
        repo.register(video2)
        repo.register(video6)
        assertEquals(list, repo.filter(filter, TestInfoRepo, list), FILTER_NON_EMPTY)
    }

    @Test fun `When a null query and empty id lists are applied to a non-empty list, verify the input list`() {
        val filter = Filter()
        val list = listOf(video1)
        repo.register(video1)
        assertEquals(list, repo.filter(filter, TestInfoRepo, list), FILTER_NON_EMPTY)
    }

    @Test fun `When only a null query is selected for a non-empty list, verify the result is the input list`() {
        val filter = Filter()
        val list = listOf(video1)
        assertEquals(list, repo.filter(filter, TestInfoRepo, list), FILTER_NON_EMPTY)
    }

    @Test fun `When only genre filtering is selected for a non-empty list, verify the result is the input list`() {
        val filter1 = Filter(listOf(genreId1))
        val filter2 = Filter(listOf(genreId2))
        assertEquals(emptyList(), repo.filter(filter1, TestInfoRepo, listOf(video2)), FILTER_NON_EMPTY)
        assertEquals(listOf(video6), repo.filter(filter2, TestInfoRepo, listOf(video6)), FILTER_EMPTY)
        assertEquals(emptyList(), repo.filter(filter2, TestInfoRepo, listOf(video2)), FILTER_NON_EMPTY)
    }

    @Test fun `When only genre filtering is selected for a non-empty list, verify the result is the empty list`() {
        val filter1 = Filter(listOf(genreId2))
        assertEquals(listOf(video6), filterMaybe(filter1, TestInfoRepo, listOf(video6)), FILTER_EMPTY)
    }

    @Test fun `When only network filtering is selected for a non-empty list, verify the result is the input list`() {
        val filter = Filter(emptyList(), listOf(networkId))
        assertEquals(listOf(video2), repo.filter(filter, TestInfoRepo, listOf(video2)), FILTER_EMPTY)
    }

    @Test fun `When network filtering is selected for a non-empty list, verify the result is the input list`() {
        val filter = Filter(emptyList(), listOf(-1))
        assertEquals(listOf(), repo.filter(filter, TestInfoRepo, listOf(video2)), FILTER_EMPTY)
    }

    @Test fun `When query and network filtering is applied to a non-empty list, verify the result`() {
        val filter = Filter(emptyList(), listOf(-1), emptyList())
        val list = listOf(video1)
        assertEquals(list, repo.filter(filter, TestInfoRepo, list), FILTER_EMPTY)
    }

    @Test fun `When only profile filtering is selected for a non-empty list, verify the result`() {
        val filter = Filter(emptyList(), emptyList(), listOf(profileId))
        val list = listOf(video4)
        assertEquals(list, repo.filter(filter, TestInfoRepo, list), FILTER_EMPTY)
    }

    @Test fun `When id filtering is selected for a non-empty list, verify the result is the input list`() {
        val filter = Filter(emptyList(), listOf(networkId), listOf(profileId))
        val filterList = listOf(video2, video4, video7)
        val resultList = listOf(video7)
        assertEquals(resultList, repo.filter(filter, TestInfoRepo, filterList), FILTER_EMPTY)
    }

    private fun getUri(): URI {
        val loader = this::class.java.classLoader
        val url = loader.getResource("read-only-files/info.txt") ?: fail(RESOURCE_FAILURE)
        return url.toURI()
    }

    companion object {
        const val FILTER_NON_EMPTY = "Expected empty result, got some entries!"
        const val FILTER_EMPTY = "Expected a non-empty result, got an empty list!"
        const val RESOURCE_FAILURE = "Could not load info resource file!"
    }
}
