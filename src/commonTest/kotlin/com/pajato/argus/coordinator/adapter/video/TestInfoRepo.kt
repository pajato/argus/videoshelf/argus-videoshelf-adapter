package com.pajato.argus.coordinator.adapter.video

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.persister.readAndPruneData
import java.net.URI

object TestInfoRepo : InfoRepo {
    override val cache: MutableMap<InfoKey, InfoWrapper> = mutableMapOf()

    private var infoUri: URI? = null

    override suspend fun injectDependency(uri: URI) {
        fun handler(exc: Throwable) { throw exc }
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, InfoWrapper.serializer(), TestInfoRepo::getKeyFromItem)
        infoUri = uri
    }

    // override fun refreshMaybe(key: InfoKey) { return }

    override suspend fun register(item: InfoWrapper) { TODO("Not yet implemented") }

    override suspend fun register(json: String) { TODO("Not yet implemented") }

    private fun getKeyFromItem(item: InfoWrapper): InfoKey = when (item) {
        is TimestampedMovie -> InfoKey(InfoType.Movie.name, item.movie.id)
        is TimestampedPerson -> InfoKey(InfoType.Person.name, item.person.id)
        is TimestampedTv -> InfoKey(InfoType.Tv.name, item.tv.id)
    }
}
