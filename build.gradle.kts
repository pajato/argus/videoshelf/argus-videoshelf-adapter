plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.argus"
version = "0.10.5"
description = "The Argus video-shelf feature, interfaces adapter (adapter) layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.kotlinx.coroutines.core)
            implementation(libs.kotlinx.serialization.json)

            implementation(libs.pajato.persister)
            implementation(libs.pajato.i18n.strings)
            implementation(libs.pajato.uri.validator)

            implementation(libs.argus.videoShelf.core)
            implementation(libs.argus.videoShelf.uc)
            implementation(libs.argus.info.adapter)
            implementation(libs.argus.info.core)
            implementation(libs.argus.info.uc)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)

            implementation(libs.tks.common.core)
            implementation(libs.tks.common.adapter)
            implementation(libs.tks.episode.core)
            implementation(libs.tks.episode.adapter)
            implementation(libs.tks.movie.core)
            implementation(libs.tks.movie.adapter)
            implementation(libs.tks.person.core)
            implementation(libs.tks.person.adapter)
            implementation(libs.tks.season.core)
            implementation(libs.tks.season.adapter)
            implementation(libs.tks.tv.core)
            implementation(libs.tks.tv.adapter)
        }
    }
}
