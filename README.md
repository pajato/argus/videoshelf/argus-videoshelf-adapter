# argus-coordinator-adapter

## Description

This project implements the [Clean Code
Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) "Interface Adapters"
layer for the coordinator feature. The project is responsible for implementing the interfaces and top
level artifacts supporting the coordinator feature.

The sole responsibility for the coordinator feature is to support the Argus application's ability to present
collections of videos matching a theme, presented in a "shelf". These themes include such things as watch next,
watch later, history, paused, finished, wait-listed and rejected.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix | Test Case Name                                                                             |
|-----------------|--------------------------------------------------------------------------------------------|
| CoordinatorRepo | When dependencies have not been injected, verify the CoordinatorError exception            |
|                 | When injecting dependencies using a flat file, verify the exception                        |
|                 | When a single, valid test shelf in injected, verify the shelf and video repo properties    |
| ShelfRepo       | When injecting a non-file URI, verify the CoordinatorError exception                       |
|                 | When injecting a valid file URI that does not exist, verify the CoordinatorError exception |
|                 | When accessing a shelf using an invalid id, verify the exception                           |
|                 | When injecting a valid file URI that is empty, verify the CoordinatorError exception       |
| VideoRepo       | When injecting an invalid repo file URI, verify the CoordinatorError exception             |
|                 | When accessing a video repo using an invalid id, verify the exception                      |
| CreateRepo      | When multiple videos are loaded, verify the cache properties                               |
|                 | When registering a video, verify the cache size                                            |
|                 | When removing a video, verify the cache size                                               |
|                 | When selecting a video, verify the behavior                                                |
| MoveShelf       | When moving the first shelf up, verify no changes                                          |
|                 | When moving the last shelf down, verify no changes                                         |
|                 | When swapping shelves up, verify changes                                                   |
|                 | When swapping shelves down, verify changes                                                 |
|                 | When moving a shelf down (2 to 3) without persisting, verify changes                       |

### Notes

The injected URI is expected to be a directory that exists and contains a file named `shelf.txt`

The `shelf.txt` file contains one or more serialized JSON `Shelf` specifications identifying a shelf ID, a shelf label
and the (unique) name of a possibly empty sibling file (created if necessary) that will contain a collection of zero or
more serialized JSON video instances.
